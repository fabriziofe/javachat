<%@page import="java.util.LinkedList"%>
<%@page import="it.phabdev.java.examples.javachat.model.User"%>
<%@page import="it.phabdev.java.examples.javachat.utils.UserManager"%>
<%@page import="it.phabdev.java.examples.javachat.utils.DateUtils"%>
<%@page import="it.phabdev.java.examples.javachat.servlet.ChatServlet"%>
<%@page import="it.phabdev.java.examples.javachat.service.ChatService"%>
<%@page import="java.util.List, it.phabdev.java.examples.javachat.model.Message"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%
    User myUser = UserManager.getMyLoggedUser(request);
    if(myUser == null){
    	response.sendRedirect("/javachat/login.jsp");
    }
    ChatServlet.initMessages(request);
    List<Message> messages = (List<Message>) request.getAttribute("messages");
    messages = messages != null ? messages : new LinkedList<Message>();
    List<User> loggedUsers = UserManager.getUserList(request);
    %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Chat</title>
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">

<link rel="stylesheet" type="text/css" href="/javachat/chat.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<script>
function populateUsers(users){
	var userContainer = $('#usersContainer');
	userContainer.empty();
	for(var i=0; i<users.length; i++){
		var name = users[i];
		var u = "<div class='chat-user'><img class='chat-avatar' src='https://bootdey.com/img/Content/avatar/avatar1.png' alt=''><div class='chat-user-name'><a href='#'>"+name+"</a></div></div>";
		$('#usersContainer').append(u);
	}
}

function populateChat(chat){
	var userContainer = $('#chatContainer');
	userContainer.empty();
	for(var i=0; i<chat.length; i++){
		var message = chat[i];
		var u = "<div class='chat-message left'><img class='message-avatar' src='https://bootdey.com/img/Content/avatar/avatar1.png' alt=''><div class='message'><a class='message-author' href='#'>"+message.user+"</a><span class='message-date'>"+message.time+"</span><span class='message-content'>"+message.message+"</span></div></div>";
		$('#chatContainer').append(u);
	}
	var d = $('#chatContainer');
	d.scrollTop(d.prop("scrollHeight"));
}

function sendMessage(){
	var messageInput = $('#messageInput');
	var messageToSend = messageInput.val();
// 	$.post('/javachat/servlet/chat', { message : messageToSend}).done().fail(function() {
// 	    alert( "error" );
// 	  });
	
	$.ajax({
        url: '/javachat/servlet/chat',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
			
        },
        data: { message : messageToSend}
    });
}

$(document).ready(function(){
	setInterval(function(){

	    $.get('/javachat/servlet/chat', function(data) {
// 	        console.log(data);  // process results here
	        window.chatData = data;
	        populateUsers(window.chatData.users);
	        populateChat(window.chatData.messages);
	    });

	},1300);
	$('#messageInput').keypress(function(e){
	      if(e.keyCode==13 && event.shiftKey){
			  e.preventDefault();
		      $('#sendButton').click();
		      $('#messageInput').val('');
	      }
    });
	var d = $('#chatContainer');
	d.scrollTop(d.prop("scrollHeight"));
	
// 	var map = {13: false, 17: false};
// 	$('#messageInput').keydown(function(e) {
// 	    if (e.keyCode in map) {
// 	        map[e.keyCode] = true;
// 	        if (map[13] && !map[17]) {
// 	        	$('#sendButton').click();
// 	 		      $('#messageInput').val('');
// 	 		      e.preventDefault();
// 	        } else {
// 	        	var e = $.Event('keypress');
// 	            e.which = 13; // Character 'A'
// 	            $('#messageInput').trigger(e);
// 	        }
// 	    }
// 	}).keyup(function(e) {
// 	    if (e.keyCode in map) {
// 	        map[e.keyCode] = false;
// 	    }
// 	});
});

</script>

</head>
<body>

<div class="container">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <strong>Chat room </strong><a href="/javachat/servlet/logout">Logout</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox chat-view">
                <div class="ibox-title">
                    <small class="pull-right text-muted">Last message:  Mon Jan 26 2015 - 18:39:23</small> Chat room panel
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-9 ">
                            <div class="chat-discussion" id="chatContainer">
<%
for(Message message : messages){
%>
                                <div class="chat-message left">
                                    <img class="message-avatar" src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="">
                                    <div class="message">
                                        <a class="message-author" href="#"> <%=message.getSender() %> </a>
                                        <span class="message-date"> <%=DateUtils.dateToString(message.getTime()) %> </span>
                                        <span class="message-content">
    										<%=message.getMessage()%>
                                            </span>
                                    </div>
                                </div>
                                <%
}
%>
                            </div>

                        </div>
                        <div class="col-md-3">
                            <div class="chat-users">


                                <div class="users-list" id="usersContainer">
                                <% for(User user : loggedUsers) { %>
                                
                                    <div class="chat-user">
                                        <img class="chat-avatar" src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="">
                                        <div class="chat-user-name">
                                            <a href="#"><%=user.getNickname() %></a>
                                        </div>
                                    </div>
                                    <%} %>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="chat-message-form">
                                <div class="form-group">
                                    <textarea id="messageInput" class="form-control message-input" name="message" placeholder="Enter message text and press enter" autofocus></textarea>
                                    <button id="sendButton" onClick="sendMessage()">Invia</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>