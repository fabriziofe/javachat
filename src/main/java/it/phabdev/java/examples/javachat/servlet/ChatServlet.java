package it.phabdev.java.examples.javachat.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.text.StringEscapeUtils;
import org.json.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import it.phabdev.java.examples.javachat.dao.MessageDAO;
import it.phabdev.java.examples.javachat.model.Message;
import it.phabdev.java.examples.javachat.model.User;
import it.phabdev.java.examples.javachat.service.ChatService;
import it.phabdev.java.examples.javachat.service.impl.ChatServiceImpl;
import it.phabdev.java.examples.javachat.utils.DateUtils;
import it.phabdev.java.examples.javachat.utils.UserManager;

public class ChatServlet extends HttpServlet {
	
	public static void initMessages(HttpServletRequest req) throws ServletException, IOException {
		ChatService service = new ChatServiceImpl();
		List<Message> messages = service.getMessages();
		req.setAttribute("messages", messages);
	}
	
	private JSONObject getJSONObject(HttpServletRequest request) throws IOException {
		StringBuffer jb = new StringBuffer();
		  String line = null;
		  try {
		    BufferedReader reader = request.getReader();
		    while ((line = reader.readLine()) != null) {
		      jb.append(line);
		    System.out.println(line);}
		  } catch (Exception e) { /*report an error*/ 
			  e.printStackTrace();
		  }
		  		
		  try {
		    JSONObject jsonObject =  HTTP.toJSONObject(jb.toString());
		    return jsonObject;
		  } catch (JSONException e) {
		    // crash and burn
		    throw new IOException("Error parsing JSON request string");
		  }
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ChatService service = new ChatServiceImpl();
		List<Message> messages = service.getMessages();
		List<User> onlineUsers = UserManager.getUserList(req);
		JSONObject result = new JSONObject();
		JSONArray userArray = new JSONArray();
		for(User user: onlineUsers) {
			userArray.put(user.getNickname());
		}
		JSONArray messageArray = new JSONArray();
		for(Message message: messages) {
			JSONObject obj = new JSONObject();
			obj.put("user", message.getSender());
			obj.put("message", message.getMessage());
			obj.put("time", DateUtils.dateToString(message.getTime()));
			messageArray.put(obj);
		}
		result.put("users", userArray);
		result.put("messages", messageArray);
		resp.setHeader("Accept", "application/json");
		resp.setHeader("Content-type", "application/json;charset=utf8");
		resp.getWriter().print(result.toString());
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doPost(req, resp);
		JSONObject json = getJSONObject(req);
		String method = json.getString("Method");
		String msg = method != null ? method.split("=")[1] : "";
		msg = URLDecoder.decode(msg, "utf8");
		User user = UserManager.getMyLoggedUser(req);
		Message message = new Message();
		Long idSender;
		message.setIdSender(user.getIdUser());
		String messageStr;
		message.setMessage(msg);
		message.setTime(Calendar.getInstance());
		message.setSender(user.getNickname());
		MessageDAO dao = new MessageDAO();
		dao.saveMessage(user,message);
		System.out.println("message is: "+msg);
		
	}

}
