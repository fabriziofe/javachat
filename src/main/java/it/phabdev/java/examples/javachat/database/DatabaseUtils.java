package it.phabdev.java.examples.javachat.database;

import javax.sql.DataSource;

// Singleton Class

public class DatabaseUtils {
	
	private static DatabaseUtils _m= null;
	
	private DataSource dataSource = null;
	
	private DatabaseUtils() {
		dataSource = MyDataSourceFactory.getMySQLDataSource();
	}
	
	public static DatabaseUtils getDatabaseUtils() {
		if(_m==null) {
			_m= new DatabaseUtils();
		}
		return _m;
	}
	
	public DataSource getDataSource() {
		return dataSource;
	}
	
	
}
