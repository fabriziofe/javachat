package it.phabdev.java.examples.javachat.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import it.phabdev.java.examples.javachat.database.DatabaseUtils;
import it.phabdev.java.examples.javachat.model.User;

public class UserDAO {

	public Boolean newUser(String email, String password, String nickname) {
		boolean done = false;
		try {
			DatabaseUtils db = DatabaseUtils.getDatabaseUtils();
			Connection connection = db.getDataSource().getConnection();
			PreparedStatement p = connection.prepareStatement("INSERT INTO USER(email,password,nickname) VALUES(?,?,?)");
			p.setString(1, email);
			p.setString(2, password);
			p.setString(3, nickname);
			int result = p.executeUpdate();
			connection.close();
			done = result >= 1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return done;
	}
	public User getUser(String email) {
		User user = null;
		try {
			DatabaseUtils db = DatabaseUtils.getDatabaseUtils();
			Connection connection = db.getDataSource().getConnection();
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery("SELECT * FROM USER WHERE email='"+email+"';");
			if(result.first()) {
				user = new User();
				Long idUser = result.getLong("idUser");
				String password = result.getString("password");
				String nickname = result.getString("nickname");
				user.setEmail(email);
				user.setIdUser(idUser);
				user.setNickname(nickname);
				user.setPassword(password);
			};
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	public User getUser(Long idUser) {
		User user = new User();
		try {
			DatabaseUtils db = DatabaseUtils.getDatabaseUtils();
			Connection connection = db.getDataSource().getConnection();
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery("SELECT * FROM USER WHERE iduser='"+idUser+"';");
			if(result.first()) {
				String email = result.getString("email");
				String password = result.getString("password");
				String nickname = result.getString("nickname");
				user.setEmail(email);
				user.setIdUser(idUser);
				user.setNickname(nickname);
				user.setPassword(password);
			};
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	
}
