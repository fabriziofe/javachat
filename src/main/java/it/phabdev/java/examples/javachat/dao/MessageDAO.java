package it.phabdev.java.examples.javachat.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import it.phabdev.java.examples.javachat.database.DatabaseUtils;
import it.phabdev.java.examples.javachat.model.Message;
import it.phabdev.java.examples.javachat.model.User;

public class MessageDAO {

	public Boolean saveMessage(User user, Message message) {
		boolean done = false;
		try {
			DatabaseUtils db = DatabaseUtils.getDatabaseUtils();
			Connection connection = db.getDataSource().getConnection();
			PreparedStatement p = connection.prepareStatement("INSERT INTO MESSAGE(sender,message,idsender) VALUES(?,?,?)");
			p.setString(1, message.getSender());
			p.setString(2, message.getMessage());
			p.setLong(3, user.getIdUser());
			int result = p.executeUpdate();
			connection.close();
			done = result >= 1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return done;
	}
	
	public List<Message> getMessages() throws SQLException{
		List<Message> messages = new LinkedList<Message>();
		
		DatabaseUtils db = DatabaseUtils.getDatabaseUtils();
		Connection connection = db.getDataSource().getConnection();
		Statement statement = connection.createStatement();
		ResultSet result = statement.executeQuery("SELECT * FROM MESSAGE;");
		boolean thereAreMoreMessages = result.first();
		while(thereAreMoreMessages) {
			Message message = new Message();
			Long idMessage = result.getLong("idMessage");
			String sender = result.getString("sender");
			String messageStr = result.getString("message");
			Timestamp timeStr = result.getTimestamp("time");
			Long idSender = result.getLong("idMessage");
			message.setIdMessage(idMessage);
			message.setIdSender(idSender);
			message.setMessage(messageStr);
			message.setSender(sender);
			message.setTime(timeStr.getTime());
			messages.add(message);
			thereAreMoreMessages = result.next();
		};
		connection.close();
		
		return messages;
	}
	
}
