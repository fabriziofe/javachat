package it.phabdev.java.examples.javachat.service;

import java.util.List;

import it.phabdev.java.examples.javachat.model.Message;

public interface ChatService {

	public List<Message> getMessages();
	
}
