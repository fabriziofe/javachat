package it.phabdev.java.examples.javachat.utils;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class RequestUtils {

	public static String getValueFromRequestPost(HttpServletRequest req, String key) {
		Map<String, String[]> parameterMap = req.getParameterMap();
		String value = parameterMap.get(key)[0];
		return value;
	}
	
}
