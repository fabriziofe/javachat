package it.phabdev.java.examples.javachat.utils;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import it.phabdev.java.examples.javachat.model.User;

public class UserManager {

	public static HashMap<String, User> getLoggedUsers(HttpServletRequest request) {
		ServletContext context = request.getSession().getServletContext();
		HashMap<String, User> users = (HashMap<String, User>) context.getAttribute("users");
		if(users == null) {
			users = new HashMap<String, User>();
		}
		return users;
	}
	public static void removeLoggedUser(HttpServletRequest request, User user) {
		ServletContext context = request.getSession().getServletContext();
		HashMap<String, User> users = (HashMap<String, User>) context.getAttribute("users");
		if(users == null) {
			users = new HashMap<String, User>();
		}
		users.remove(user.getNickname());
		request.getSession().setAttribute("loggedUser",null);
		context.setAttribute("users", users);
	}
	
	public static void addLoggedUser(HttpServletRequest request, User user) {
		ServletContext context = request.getSession().getServletContext();
		HashMap<String, User> users = (HashMap<String, User>) context.getAttribute("users");
		if(users == null) {
			users = new HashMap<String, User>();
		}
		users.put(user.getNickname(), user);
		request.getSession().setAttribute("loggedUser",user);
		context.setAttribute("users", users);
	}
	public static List<User> getUserList(HttpServletRequest request){
		 HashMap<String, User> loggedUsers = getLoggedUsers(request);
		 List<User> result = new LinkedList<User>();
		 for(String key: loggedUsers.keySet()) {
			 User user = loggedUsers.get(key);
			 result.add(user);
		 }
		 return result;
	}
	
	public static User getMyLoggedUser(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("loggedUser");
		return user;
	}
	
}
