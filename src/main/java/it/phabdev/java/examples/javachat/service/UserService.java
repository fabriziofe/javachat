package it.phabdev.java.examples.javachat.service;

import it.phabdev.java.examples.javachat.model.User;

public interface UserService {

	public User checkLogin(String email, String password);
	public Boolean signupNewUser(String email, String password, String nickname);
	
}
