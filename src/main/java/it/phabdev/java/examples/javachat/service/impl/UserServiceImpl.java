package it.phabdev.java.examples.javachat.service.impl;

import it.phabdev.java.examples.javachat.dao.UserDAO;
import it.phabdev.java.examples.javachat.model.User;
import it.phabdev.java.examples.javachat.service.UserService;

public class UserServiceImpl implements UserService {

	@Override
	public User checkLogin(String email, String password) {
		UserDAO userDao = new UserDAO();
		User user = userDao.getUser(email);
		String usrPassword = user!= null ? user.getPassword(): null;
		if(usrPassword != null && usrPassword.equals(password)) {
			return user;
		} else {
			return null;
		}
	}
	
	@Override
	public Boolean signupNewUser(String email, String password, String nickname) {
		UserDAO userDao = new UserDAO();
		Boolean done = userDao.newUser(email, password, nickname);
		return done;
	}

}
