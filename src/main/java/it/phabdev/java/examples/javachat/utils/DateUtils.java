package it.phabdev.java.examples.javachat.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateUtils {

	public static String dateToString(Calendar c) {
		String result = "";
		
		SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd yyyy HH:mm");
		result = sdf.format(c.getTime());
		return result;
	}
	
}
