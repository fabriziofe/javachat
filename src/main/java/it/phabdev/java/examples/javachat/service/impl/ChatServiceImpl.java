package it.phabdev.java.examples.javachat.service.impl;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import it.phabdev.java.examples.javachat.dao.MessageDAO;
import it.phabdev.java.examples.javachat.model.Message;
import it.phabdev.java.examples.javachat.service.ChatService;

public class ChatServiceImpl implements ChatService {

	@Override
	public List<Message> getMessages() {
		MessageDAO dao = new MessageDAO();
		List<Message> messages = null;
		try {
			messages = dao.getMessages();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return messages;
	}

}
