package it.phabdev.java.examples.javachat.servlet;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.phabdev.java.examples.javachat.model.User;
import it.phabdev.java.examples.javachat.service.UserService;
import it.phabdev.java.examples.javachat.service.impl.UserServiceImpl;
public class SimpleServlet extends HttpServlet {
	private static final long serialVersionUID = -4751096228274971485L;
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		if(email == null || email.isEmpty() || password == null || password.isEmpty()) {
			response.getWriter().println("Check values");
			return;
		}
		UserService userService = new UserServiceImpl();
		User user = userService.checkLogin(email, password);
		if(user!= null) {
			response.getWriter().println("Hello "+user.getNickname()+"!");
		} else {
			response.getWriter().println("No User Found");
		}
	}
	@Override
	public void init() throws ServletException {
		System.out.println("Servlet " + this.getServletName() + " has started");
	}
	@Override
	public void destroy() {
		System.out.println("Servlet " + this.getServletName() + " has stopped");
	}
}