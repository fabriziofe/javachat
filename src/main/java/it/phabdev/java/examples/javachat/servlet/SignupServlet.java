package it.phabdev.java.examples.javachat.servlet;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.phabdev.java.examples.javachat.service.UserService;
import it.phabdev.java.examples.javachat.service.impl.UserServiceImpl;
import it.phabdev.java.examples.javachat.utils.RequestUtils;
public class SignupServlet extends HttpServlet {
	private static final long serialVersionUID = -4751096228274971485L;
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		response.sendRedirect("/javachat/signup.jsp");
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = RequestUtils.getValueFromRequestPost(request, "email");
		String password = RequestUtils.getValueFromRequestPost(request, "password");
		String nickname = RequestUtils.getValueFromRequestPost(request, "nickname");
		UserService userService = new UserServiceImpl();
		Boolean done = userService.signupNewUser(email, password, nickname);
		if(done) {
//			response.getWriter().println("User is not null");
			response.sendRedirect("/javachat/chat.jsp");
		} else {
//			response.getWriter().println("User is null");
			request.getSession().setAttribute("error", true);
			response.sendRedirect("/javachat/signup.jsp");
		}
	}
	
	@Override
	public void init() throws ServletException {
		System.out.println("Servlet " + this.getServletName() + " has started");
	}
	@Override
	public void destroy() {
		System.out.println("Servlet " + this.getServletName() + " has stopped");
	}
}