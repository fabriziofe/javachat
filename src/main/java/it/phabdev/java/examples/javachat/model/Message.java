package it.phabdev.java.examples.javachat.model;

import java.util.Calendar;

public class Message {

	private Long idMessage;
	private String sender;
	private String message;
	private Calendar time;
	private Long idSender;

	public Message() {
	}

	public Long getIdMessage() {
		return idMessage;
	}

	public void setIdMessage(Long idMessage) {
		this.idMessage = idMessage;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Calendar getTime() {
		return time;
	}

	public void setTime(Long timestamp) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(timestamp);
		this.setTime(c);
	}

	public void setTime(Calendar time) {
		this.time = time;
	}

	public Long getIdSender() {
		return idSender;
	}

	public void setIdSender(Long idSender) {
		this.idSender = idSender;
	}

}
