
package it.phabdev.java.examples.javachat.database;

import javax.sql.DataSource;

import com.mysql.cj.jdbc.MysqlDataSource;

public class MyDataSourceFactory {

	private static final String MYSQL_DB_DRIVER_CLASS = "com.mysql.jdbc.Driver";
	private static final String MYSQL_DB_URL = "jdbc:mysql://localhost:3342/javachat?serverTimezone=Europe/Rome";
	private static final String MYSQL_DB_USERNAME = "root";
	private static final String MYSQL_DB_PASSWORD = "password";

	public static DataSource getMySQLDataSource() {
		MysqlDataSource mysqlDS = new MysqlDataSource();
		mysqlDS.setURL(MYSQL_DB_URL);
		mysqlDS.setUser(MYSQL_DB_USERNAME);
		mysqlDS.setPassword(MYSQL_DB_PASSWORD);
		return mysqlDS;
	}

}
