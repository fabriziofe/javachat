package it.phabdev.java.examples.javachat.servlet;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.phabdev.java.examples.javachat.model.User;
import it.phabdev.java.examples.javachat.service.UserService;
import it.phabdev.java.examples.javachat.service.impl.UserServiceImpl;
import it.phabdev.java.examples.javachat.utils.RequestUtils;
import it.phabdev.java.examples.javachat.utils.UserManager;
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = -4751096228274971485L;
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("loggedUser");
		if(user!=null) {
			UserManager.removeLoggedUser(request, user);
		}
		response.sendRedirect("/javachat/login.jsp");
	}
	
	@Override
	public void init() throws ServletException {
		System.out.println("Servlet " + this.getServletName() + " has started");
	}
	@Override
	public void destroy() {
		System.out.println("Servlet " + this.getServletName() + " has stopped");
	}
}